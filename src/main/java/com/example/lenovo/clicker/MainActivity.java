package com.example.lenovo.clicker;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.view3)
    View view3;
    @BindView(R.id.view4)
    View view4;

    static final String[] colors = {"#800000", "#993333", "#008000", "#cc6600", "#000080",
            "#9966cc", "#f80000", "#00f800", "#0000f8", "#777777"};
    static String[] tempColors = {"#800000", "#993333", "#008000", "#cc6600"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        view1.setBackgroundColor( Color.parseColor(tempColors[0]));
        view2.setBackgroundColor( Color.parseColor(tempColors[1]));
        view3.setBackgroundColor( Color.parseColor(tempColors[2]));
        view4.setBackgroundColor( Color.parseColor(tempColors[3]));
    }

    @OnClick(R.id.view1)
    public void onClick1() {
        String[] tempColors = randomizeColors(1);
        view2.setBackgroundColor( Color.parseColor(tempColors[1]));
        view3.setBackgroundColor( Color.parseColor(tempColors[2]));
        view4.setBackgroundColor( Color.parseColor(tempColors[3]));
    }
    @OnClick(R.id.view2)
    public void onClick2() {

        String[] tempColors = randomizeColors(2);
        view1.setBackgroundColor( Color.parseColor(tempColors[0]));
        view3.setBackgroundColor( Color.parseColor(tempColors[2]));
        view4.setBackgroundColor( Color.parseColor(tempColors[3]));
    }

    @OnClick(R.id.view3)
    public void onClick3() {
        String[] tempColors = randomizeColors(3);
        view2.setBackgroundColor( Color.parseColor(tempColors[1]));
        view1.setBackgroundColor( Color.parseColor(tempColors[0]));
        view4.setBackgroundColor( Color.parseColor(tempColors[3]));
    }
    @OnClick(R.id.view4)
    public void onClick4() {
        String[] tempColors = randomizeColors(4);
        view2.setBackgroundColor( Color.parseColor(tempColors[1]));
        view3.setBackgroundColor( Color.parseColor(tempColors[2]));
        view1.setBackgroundColor( Color.parseColor(tempColors[0]));
    }

    public static String[] randomizeColors(int viewNumber) {
        String tempColor = 	"#FFFFFF";
        boolean hasColor;
        for(int i = 0; i < tempColors.length; i++){
            hasColor = true;
            if(i != (viewNumber-1)) {
                while (hasColor) {
                    hasColor = false;
                    tempColor = colors[(int) (Math.random() * (9))];
                    for (int j = 0; j < tempColors.length; j++) {
                        if (tempColors[j].equalsIgnoreCase(tempColor)) {
                            hasColor = true;
                            break;
                        }
                    }
                }
                tempColors[i] = tempColor;
            }
        }
        return tempColors;
    }
}
